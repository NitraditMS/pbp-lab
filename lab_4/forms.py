from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = "__all__"

        # widgets = {
        #     'title' : forms.TextInput(attrs={'class': 'input', 'placehoolder': 'Messege title'}),
        #     'to' : forms.TextInput(attrs={'class': 'input', 'placehoolder': 'To'}),
        #     'frm' : forms.TextInput(attrs={'class': 'input', 'placehoolder': 'From'}),
        #     'messege' : forms.Textarea(attrs={'class': 'textarea', 'rows': 10, 'placehoolder': 'Enter text here'})
        # }
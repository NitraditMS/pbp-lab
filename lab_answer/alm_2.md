[PBP lab-2 question](https://gitlab.com/PBP-2021/pbp-lab/-/blob/master/lab_instruction/lab_2/README.md)

**1.  Apakah perbedaan antara JSON dan XML?**

*JSON (JavaScript Object Notation)*
- Berbasis JavaScript dan menggunakan notasi objeknya
- Dapat mengimplementasikan array
- File lebih mudah untuk dibaca dibandingkan dengan XML
- Hanya dapat menggunakan encoding UTF-8

*XML (Extensible Markup Language)*
- Menggunakan sistem data terstruktur dan tag
- Tidak dapat mengimplementasikan array
- File lebih sulit untuk dibaca dan dimengerti
- Dapat menggunakan berbagai macan encoding

**2.  Apakah perbedaan antara HTML dan XML?**

*HTML (Hypertext Markup Language)*
- Struktur data lebih mudah dilihat
- Digunakan untuk menampilkan data
- Menggunakan tag-tag yang sudah terdefinisi

*XML*
- Data dapat dipindahkan kapan saja tanpa mempengaruhi tampilan pada saat itu
- Digunakan untuk menyimpan dan mengirim/memindahkan data
- Tag didefinisikan oleh pengguna